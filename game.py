from random import randint
guesses = []
result = ""


user_name = input("Hi! What is your name?")

for guess_num in range(5):
    month_num = randint(1,12)
    year_num = randint(1924, 2004)

    print(f"Guess {guess_num + 1}", (user_name), "were you born in", month_num, "/", year_num, "?")

    guess = input("yes or no?")

    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess_num == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again")
